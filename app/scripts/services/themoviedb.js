'use strict';

/**
 * @ngdoc service
 * @name uiDeveloperTestApp.theMovieDB
 * @description
 * # theMovieDB
 * Factory in the uiDeveloperTestApp.
 */
angular.module('uiDeveloperTestApp')
        .factory('theMovieDBService', function ($resource) {

            var apiKey = 'cb82f8dcac9f0a6c269d1fcfa4c7d191';

            return {
                person_search: function () {
                    var url = 'http://api.themoviedb.org/3/search/person';

                    return $resource(url,
                            {api_key: apiKey, callback: 'JSON_CALLBACK', query: ''},
                    {get: {method: 'JSONP', requestType: 'json'}}
                    );
                },
                
                person_get: function () {
                    var url = 'http://api.themoviedb.org/3/person/:id';

                    return $resource(url,
                            {api_key: apiKey, callback: 'JSON_CALLBACK', id:'', append_to_response:'movie_credits'},
                    {get: {method: 'JSONP', requestType: 'json'}}
                    );
                },
                
                search_term: '',
                cached_search: [],
            };
        });
