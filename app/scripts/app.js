'use strict';

/**
 * @ngdoc overview
 * @name uiDeveloperTestApp
 * @description
 * # uiDeveloperTestApp
 *
 * Main module of the application.
 */
angular
        .module('uiDeveloperTestApp', [
            'ngAnimate',
            'ngCookies',
            'ngResource',
            'ngRoute',
            'ngSanitize',
            'ngTouch',
        ])
        .config(function ($routeProvider) {
            $routeProvider
                    .when('/search/', {
                        templateUrl: 'views/main.html',
                        controller: 'MainCtrl'
                    })
                    .when('/about', {
                        templateUrl: 'views/about.html',
                        controller: 'AboutCtrl'
                    })
                    .when('/detail/:id', {
                      templateUrl: 'views/detail.html',
                      controller: 'DetailCtrl'
                    })
                    .otherwise({
                        redirectTo: '/search'
                    });
        });
