'use strict';

/**
 * @ngdoc function
 * @name uiDeveloperTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the uiDeveloperTestApp
 */
angular.module('uiDeveloperTestApp')
        .controller('MainCtrl', ['$scope', 'theMovieDBService', function ($scope, theMovieDBService) {
                $scope.sSearchTerm = theMovieDBService.search_term || '';
                $scope.aoPeople = theMovieDBService.cached_search || [];

                $scope.search = function () {
                    if (!$scope.sSearchTerm || $scope.sSearchTerm.length < 3)
                        return;
                    
                    if(theMovieDBService.cached_search[$scope.sSearchTerm]){
                       $scope.aoPeople = theMovieDBService.cached_search[$scope.sSearchTerm];
                       return;
                    }

                    var r = theMovieDBService.person_search();

                    r.get({query: $scope.sSearchTerm}, function (result) {

//                        theMovieDBService.search_term = $scope.sSearchTerm;
                        $scope.aoPeople = result.results;
                        console.log(result.results);
                    });
                }

                $scope.clean = function () {
                    $scope.sSearchTerm = '';
                    $scope.aoPeople = [];
                }

                $scope.search();

                $scope.$watch('sSearchTerm', function (val) {
                    theMovieDBService.search_term = val;
                });
                
                $scope.$watch('aoPeople', function (val) {
                    theMovieDBService.cached_search[$scope.sSearchTerm] = val;
                });

            }]);
