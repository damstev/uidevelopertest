'use strict';

/**
 * @ngdoc function
 * @name uiDeveloperTestApp.controller:DetailCtrl
 * @description
 * # DetailCtrl
 * Controller of the uiDeveloperTestApp
 */
angular.module('uiDeveloperTestApp')
        .controller('DetailCtrl', ['$scope', '$routeParams', 'theMovieDBService', '$filter', function ($scope, $routeParams, theMovieDBService, $filter) {
                
                var id = $routeParams.id;
        
                $scope.oPerson = {};
                $scope.aoMovies = [];
                $scope.sSearchMovie = '';
                
                var normalize = $filter('normalize');

                ///function used to order movies by release date
                function compare(a, b) {
                    if (a.release_date < b.release_date)
                        return -1;
                    if (a.release_date > b.release_date)
                        return 1;
                    return 0;
                }

                var r = theMovieDBService.person_get();
                r.get({id: id}, function (result) {
                    console.log(result);

                    $scope.oPerson = result;
                    $scope.aoMovies = result.movie_credits.cast.sort(compare);                    
                });


                $scope.fnSearchMovie = function () {
                    $scope.aoMovies = _.filter( $scope.oPerson.movie_credits.cast, function(oMovie){
                        return normalize(oMovie.title).indexOf(normalize($scope.sSearchMovie)) > -1;
                    });
                }

            }]);
