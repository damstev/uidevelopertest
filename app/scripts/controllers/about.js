'use strict';

/**
 * @ngdoc function
 * @name uiDeveloperTestApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the uiDeveloperTestApp
 */
angular.module('uiDeveloperTestApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
