'use strict';

/**
 * @ngdoc filter
 * @name uiDeveloperTestApp.filter:normalize
 * @function
 * @description
 * # normalize
 * Filter in the uiDeveloperTestApp.
 */
angular.module('uiDeveloperTestApp')
        .filter('normalize', function () {
            return function (input) {

                if (!input)
                    return "";

                input = input.replace(/\W+/g, '');
                return input.toLowerCase();
            }
        });
