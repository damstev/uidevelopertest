'use strict';

describe('Service: theMovieDB', function () {

  // load the service's module
  beforeEach(module('uiDeveloperTestApp'));

  // instantiate service
  var theMovieDB;
  beforeEach(inject(function (_theMovieDB_) {
    theMovieDB = _theMovieDB_;
  }));

  it('should do something', function () {
    expect(!!theMovieDB).toBe(true);
  });

});
